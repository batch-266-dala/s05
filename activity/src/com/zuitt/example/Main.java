package com.zuitt.example;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Phonebook record = new Phonebook();
        Contact firstContact = new Contact();
        Contact secondContact = new Contact();

        firstContact.setName("Martin Luther");
        firstContact.setContactNumber("+639023192121");
        firstContact.setAddress("Blk 4 Lot 119 Eastbank WestBank Anywhere");

        secondContact.setName("Diether Magdayao");
        secondContact.setContactNumber("+63904129281293");
        secondContact.setAddress("Blk 3 Lot 120 Soutbank Northbank Anywhere");

        record.setContacts(firstContact);
        record.setContacts(secondContact);

        if(record.getContacts().isEmpty()){
            System.out.println("The phonebook is empty");
        } else {
            record.getContacts().forEach(Details -> {
                System.out.println("----------");
                System.out.println(Details.getName());
                System.out.println("-<has the following details>-");
                System.out.println("Contact Number: " + Details.getContactNumber());
                System.out.println("Address: " + Details.getAddress());
                System.out.println("----------");

            });
        }
    }
}
